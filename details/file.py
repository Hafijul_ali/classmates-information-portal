import os
import random
fln=['amelia', 'ava', 'elijah', 'emma', 'liam', 'lucas', 'noah', 'oliver', 'olivia', 'sophia']
ss=['Broad River Grammar School','Seal Coast High School','Sunny Coast Elementary','Seal Bay College','Riverdale High','Greenfield School']
hss=['Einstein Academy', 'Seal Gulch Grammar School', 'Green Valley School ', 'Mountain Oak Institute', 'Riverbank School', 'South Fork Charter School']
uni=['Winters Technical School', 'Sacred Heart Charter School', 'Little Valley Institute', 'Blue River College', 'Mountain Oak School', 'Fortuna School ']
 
for n in fln:

    code="""
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
            <title>{0}</title>
        </head>
        <body>
            <div class="card text-center" style="margin: 10vw;">
                <div class="card-header">{0}</div>
                <div class="card-body">
                  <table class="table table-hover table-responsive justify-content-center" style="margin:auto; width: 50% !important;">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name of Institue</th>
                          <th scope="col">Education</th>
                          <th scope="col">Percentile</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>{1}</td>
                          <td>Secondary School</td>
                          <td>{2} %</td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>{3}</td>
                          <td>Higher Secondary School</td>
                          <td>{4} %</td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>{5}</td>
                          <td>University</td>
                          <td>{6} GPA</td>
                        </tr>
                      </tbody>
                  </table>
                </div>
                <div class="card-footer text-muted"></div>
            </div>
            <a href="../index.html" class="btn btn-primary" style="float: right; margin: 10vw;">Back</a>
        </body>
    </html>
    """.format(n.title(),random.choice(ss),round(random.uniform(50,100),2),random.choice(hss),round(random.uniform(50,100),2),random.choice(uni),round(random.uniform(5,10),2))
    with open('{0}.html'.format(n),"a") as f:
        f.write(code)


